
# Grader

Simulate a simple grader in `Gazebo` and `Rviz`

![grader](https://gitlab.com/ShadeSWD/grader_control/-/wikis/uploads/8eb2dc6e43f85fd60bc490a1b32bf13a/photo1656524978.jpeg)

This model has 3 markers on the bottom edge of the blade, which are displayed on the plot.
## Install

```bash
# Move to catkin_ws
cd ~/catkin_ws/
source ./devel/setup.bash

# Build
cd <catkin_ws>/src
catkin_make
```

## Run

```bash
source ./devel/setup.bash

# In rviz and gazebo
roslaunch grader_simulator grader.launch

```

## Control

```
- Up        speed +
- Down      speed -
- Left      turn wheels a bit to the left
- Right     turn wheels a bit to the right
- S         stop
- Q         blade effort up
- W         blade effort down
- E         blade turns a bit left
- R         blade turns a bit right
- T         blade tilts a bit forward
- Y         blade tilts a bit backward
- C         all params to zero
```

## Dimensions
```
Dimensions are set in meters:

- Length:               3.250
- Width:                3.010
- Wheelbase (length):   4.000
- Wheelbase (width):    2.000
- Wheel diameter:       1.000
- Wheel width:          0.420
- Blade length:         3.010
- Blade width:          0.570
- Blade thickness:      0.018
```
