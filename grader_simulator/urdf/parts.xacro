<?xml version="1.0"?>
<robot name="grader" xmlns:xacro="http://www.ros.org/wiki/xacro">

<xacro:include filename="$(find grader_simulator)/urdf/functions.xacro" />

<!--    Base_link-->
    <xacro:macro name="base_link">
        <link name="base_link">
            <visual>
                <geometry>
                    <box size="0 0 0"/>
                </geometry>
            </visual>
        </link>
    </xacro:macro>

    <!--    Wheelbase-->
    <xacro:macro name="wheelbase">
        <link name="body">
            <visual>
                <geometry>
                    <box size="${wheelbase_length} ${wheelbase_width} ${wheelbase_height}"/>
                </geometry>
                <material name="yellow"/>
                <origin xyz="0 0 ${wheelbase_height/2}"/>
            </visual>
            <inertial>
                <xacro:box_inertial mass="5000" length="${wheelbase_length}" width="${wheelbase_width}" height="${wheelbase_height}"/>
            </inertial>
            <collision>
                <geometry>
                    <box size="0 0 0"/>
                    <origin xyz="0 0 ${wheelbase_height/2}"/>
                </geometry>
            </collision>
    </link>

    <joint name="base_link_to_body" type="fixed">
        <parent link="base_link"/>
        <child link="body"/>
        <origin rpy="0 0 0" xyz="0 0 ${wheel_diameter/2}"/>
    </joint>

    <!-- Wheelbase camera-->
    <gazebo reference="body">
        <mu1 value="200.0"/>
        <mu2 value="100.0"/>
        <kp value="10000000.0" />
        <kd value="1.0" />
        <material>Gazebo/Yellow</material>
        <sensor type="camera" name="camera1">
            <always_on>true</always_on>
            <visualize>true</visualize>
            <update_rate>30.0</update_rate>
            <camera name="head">
                <horizontal_fov>1.3962634</horizontal_fov>
                <image>
                    <width>800</width>
                    <height>800</height>
                    <format>R8G8B8</format>
                </image>
                <clip>
                    <near>0.02</near>
                    <far>300</far>
                </clip>
                <noise>
                    <type>gaussian</type>
                    <mean>0.0</mean>
                    <stddev>0.007</stddev>
                </noise>
            </camera>

            <plugin name="camera_controller" filename="libgazebo_ros_camera.so">
                <alwaysOn>true</alwaysOn>
                <updateRate>0.0</updateRate>
                <cameraName>grader/camera1</cameraName>
                <imageTopicName>image_raw</imageTopicName>
                <cameraInfoTopicName>camera_info</cameraInfoTopicName>
                <frameName>camera_link</frameName>
                <hackBaseline>0.07</hackBaseline>
                <distortionK1>0.0</distortionK1>
                <distortionK2>0.0</distortionK2>
                <distortionK3>0.0</distortionK3>
                <distortionT1>0.0</distortionT1>
                <distortionT2>0.0</distortionT2>
            </plugin>
        </sensor>
    </gazebo>
    </xacro:macro>

    <!--    Antenna-->
    <xacro:macro name="antenna">
        <link name="antenna">
            <visual>
                <geometry>
                    <cylinder length="${antenna_length}" radius="${antenna_radius}"/>
                </geometry>
                <material name="green"/>
                <origin xyz="0 0 ${antenna_length/2}"/>
            </visual>
            <inertial>
                 <xacro:cylinder_inertial mass="10" radius="${antenna_radius}" length="${antenna_length}"/>
            </inertial>
            <collision>
                <geometry>
                    <cylinder length="${antenna_length}" radius="${antenna_radius}"/>
                </geometry>
            </collision>
        </link>

        <joint name="body_to_antenna" type="fixed">
            <parent link="body"/>
            <child link="antenna"/>
            <origin rpy="0 ${antenna_angle} 0" xyz="${-wheelbase_length/4} 0 ${wheelbase_height}"/>
        </joint>

        <gazebo reference="antenna">
            <material>Gazebo/Green</material>
        </gazebo>
    </xacro:macro>

    <!--    Wheels-->
    <xacro:macro name="rear_wheel" params="prefix side">
          <link name="${prefix}_rear_wheel">
                <visual>
                    <geometry>
                        <cylinder radius="${wheel_diameter/2}" length="${wheel_width}" />
                    </geometry>
                    <material name="blue"/>
                    <origin rpy="1.570796 0 0" xyz="0 0 0"/>
                </visual>
                <inertial>
                    <xacro:cylinder_inertial mass="50" radius="${wheel_diameter/2}" length="${wheel_width}"/>
                </inertial>
                <collision>
                    <geometry>
                        <cylinder radius="${wheel_diameter/2}" length="${wheel_width}" />
                    </geometry>
                    <origin rpy="1.570796 0 0" xyz="0 0 0"/>
                </collision>
          </link>

        <joint name="body_to_${prefix}_rear_wheel" type="continuous">
            <parent link="body"/>
            <child link="${prefix}_rear_wheel"/>
            <origin rpy="0 0 0" xyz="${-wheelbase_length/2} ${(wheelbase_width/2+wheel_width/2)*side} 0"/>
            <axis xyz="0 1 0"/>
            <limit effort="10000.0" velocity="3"/>
            <dynamics damping="0.0" friction="0.0"/>
        </joint>

        <gazebo reference="${prefix}_rear_wheel">
            <mu1 value="200.0"/>
            <mu2 value="100.0"/>
            <material>Gazebo/Blue</material>
        </gazebo>
    </xacro:macro>

    <xacro:macro name="front_wheel" params="prefix side">
        <link name="${prefix}_front_wheel_holder">
            <visual>
                <geometry>
                    <box size="0.0001 0.0001 0.0001"/>
                </geometry>
            </visual>
            <inertial>
                <xacro:box_inertial mass="0.01" height="0" length="0" width="0"/>
            </inertial>
            <collision>
                <geometry>
                    <box size="0 0 0"/>
                </geometry>
            </collision>
        </link>

        <joint name="body_to_${prefix}_front_wheel_holder" type="revolute">
            <parent link="body"/>
            <child link="${prefix}_front_wheel_holder"/>
            <origin rpy="0 0 0" xyz="${wheelbase_length/2} ${(wheelbase_width/2+wheel_width/2)*side} 0"/>
            <limit effort="10000.0" lower="-1.0" upper="1.0" velocity="100"/>
            <axis xyz="0 0 1"/>
            <dynamics damping="0.0" friction="0.0"/>
        </joint>

        <link name="${prefix}_front_wheel">
            <visual>
                <geometry>
                    <cylinder radius="${wheel_diameter/2}" length="${wheel_width}" />
                </geometry>
                <material name="blue"/>
                <origin rpy="1.570796 0 0" xyz="0 0 0"/>
            </visual>
            <inertial>
                <xacro:cylinder_inertial mass="50" radius="${wheel_diameter/2}" length="${wheel_width}"/>
            </inertial>
            <collision>
                <geometry>
                    <cylinder radius="${wheel_diameter/2}" length="${wheel_width}" />
                </geometry>
                <origin rpy="1.570796 0 0" xyz="0 0 0"/>
            </collision>
        </link>

        <joint name="${prefix}_front_wheel_holder_to_${prefix}_front_wheel" type="continuous">
            <parent link="${prefix}_front_wheel_holder"/>
            <child link="${prefix}_front_wheel"/>
            <origin rpy="0 0 0" xyz="0 0 0"/>
            <axis xyz="0 1 0"/>
            <limit effort="1000.0" velocity="10.0"/>
            <dynamics damping="0.0" friction="0.0"/>
        </joint>

        <gazebo reference="${prefix}_front_wheel">
            <mu1 value="100.0"/>
            <mu2 value="100.0"/>
            <material>Gazebo/Blue</material>
        </gazebo>
    </xacro:macro>

    <!--    Blade-->
    <xacro:macro name="blade">
        <link name="blade_holder">
            <visual>
                <geometry>
                    <box size="0.0001 0.0001 0.0001" />
                </geometry>
                <origin rpy="0 0 0" xyz="0 0 0"/>
            </visual>
            <inertial>
                <xacro:box_inertial mass="30" height="0" length="0" width="0"/>
            </inertial>
            <collision>
                <geometry>
                    <box size="0 0 0"/>
                </geometry>
            </collision>
        </link>

        <joint name="body_to_blade_holder" type="prismatic">
            <parent link="body"/>
            <child link="blade_holder"/>
            <axis xyz="0 0 1"/>
            <limit effort="1000.0" lower="${-1*wheel_diameter/2}" upper="${blade_width/2}" velocity="5"/>
            <origin rpy="0 0 0" xyz="0 0 0"/>
            <dynamics damping="0.0" friction="0.0"/>
        </joint>

        <gazebo reference="blade_holder">
            <gravity>False</gravity>
            <material>Gazebo/Red</material>
        </gazebo>

        <link name="blade_angle_keeper">
            <visual>
                <geometry>
                    <box size="0.0001 0.0001 0.0001" />
                </geometry>
                <origin rpy="0 0 0" xyz="0 0 0"/>
            </visual>
            <inertial>
                <xacro:box_inertial mass="0.1" height="0" length="0" width="0"/>
            </inertial>
            <collision>
                <geometry>
                    <box size="0 0 0"/>
                </geometry>
            </collision>
        </link>

        <joint name="blade_holder_to_blade_angle_keeper" type="revolute">
            <parent link="blade_holder"/>
            <child link="blade_angle_keeper"/>
            <axis xyz="0 1 0"/>
            <limit effort="1000.0" lower="-0.5" upper="0.5" velocity="2"/>
            <origin rpy="0 0 0" xyz="0 0 0"/>
            <dynamics damping="0.0" friction="0.0"/>
        </joint>

        <gazebo reference="blade_angle_keeper">
            <gravity>False</gravity>
            <material>Gazebo/Red</material>
        </gazebo>

        <link name="blade">
            <visual>
                <geometry>
                    <box size="${blade_thickness} ${blade_length} ${blade_width}" />
                </geometry>
                <material name="red"/>
                <origin rpy="0 0 0" xyz="0 0 0"/>
            </visual>
            <inertial>
                <xacro:box_inertial mass="0.1" length="${blade_thickness}" width="${blade_length}" height="${blade_width}"/>
            </inertial>
            <collision>
                <geometry>
                    <box size="0 0 0"/>
                </geometry>
            </collision>
        </link>

        <joint name="blade_angle_keeper_to_blade" type="revolute">
            <parent link="blade_angle_keeper"/>
            <child link="blade"/>
            <axis xyz="0 0 1"/>
            <limit effort="1000.0" lower="-0.5" upper="0.5" velocity="2"/>
            <origin rpy="0 0 0" xyz="0 0 0"/>
            <dynamics damping="0.0" friction="0.0"/>
        </joint>

        <gazebo reference="blade">
            <gravity>False</gravity>
            <material>Gazebo/Red</material>
        </gazebo>
    </xacro:macro>

<!--    Blade markers-->
    <xacro:macro name="blade_marker" params="name side">
        <link name="${name}_blade_marker">
            <visual>
                <geometry>
                    <box size="0 0 0" />
                </geometry>
                <origin rpy="0 0 0" xyz="0 0 0"/>
            </visual>
            <inertial>
                <xacro:box_inertial mass="0.0001" height="0" length="0" width="0"/>
            </inertial>
            <collision>
                <geometry>
                    <box size="0 0 0"/>
                </geometry>
            </collision>
        </link>

        <joint name="blade_to_${name}_blade_marker" type="continuous">
            <parent link="blade"/>
            <child link="${name}_blade_marker"/>
            <axis xyz="0 1 0"/>
            <limit effort="0" lower="0" upper="0" velocity="0"/>
            <origin rpy="0 0 0" xyz="0 ${side} ${-1*blade_width/2}"/>
            <dynamics damping="0.0" friction="0.0"/>
        </joint>

        <gazebo reference="${name}_blade_marker">
            <gravity>False</gravity>
            <material>Gazebo/Blue</material>
        </gazebo>
    </xacro:macro>
</robot>