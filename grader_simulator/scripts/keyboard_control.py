#!/usr/bin/env python
import getch
import rospy
from std_msgs.msg import String
from std_msgs.msg import Int8


def keys():
    pub = rospy.Publisher('/grader/control_keys', Int8, queue_size=10)  # "key" is the publisher name
    rospy.init_node('keypress', anonymous=True)
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        k = ord(
            getch.getch())  # this is used to convert the keypress event in the keyboard to an ord value
        if ((k >= 65) & (k <= 68) | (k == 115) | (k == 113) | (k == 119) | (k == 101) | (k == 114) | (k == 116) | (
                k == 121) | (k == 99)):  # to filter only the up , down ,left , right key
            rospy.loginfo(str(k))  # to print on  terminal
            pub.publish(k)  # to publish
        rospy.loginfo(str(k))

        rate.sleep()


# s=115, q=113, w=119, e=101, r=114, t=116, y=121, c=99

if __name__ == '__main__':
    try:
        keys()
    except rospy.ROSInterruptException:
        pass
