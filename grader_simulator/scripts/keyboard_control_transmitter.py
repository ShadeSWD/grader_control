#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64, Int8

TURN_LIMIT = 0.6
TURN_BIT = 0.1
REAR_WHEELS_VELOCITY_LIMIT = 3
WHEELS_VELOCITY_BIT = 0.1

LOW_BLADE_HEIGHT_VELOCITY = -0.3
HIGH_BLADE_HEIGHT_VELOCITY = 0.3
DEFAULT_BLADE_VELOCITY = 0
BLADE_VELOCITY_BIT = 0.05


def correct_data(data, lower_limit, upper_limit):
    if data > upper_limit:
        data = upper_limit
    if data < lower_limit:
        data = lower_limit
    return data


class KeyboardTransmitter:
    def __init__(self):
        rospy.init_node('keyboard_control_transmitter', anonymous=True)
        # Subscribe for keyboard control
        rospy.Subscriber("/grader/control_keys", Int8, self.update_key)
        self.key = ''

        self.rate = rospy.Rate(10)

        self.rear_wheels_velocity = 0
        self.turn_angle = 0
        self.blade_height_velocity = 0
        self.blade_tilt = 0
        self.blade_angle = 0
        self.turn_direction = 1

        self.rear_wheels_velocity_pub = rospy.Publisher("/grader/controls/rear_wheels_velocity", Float64, queue_size=10)
        self.turn_angle_pub = rospy.Publisher("/grader/controls/turn_angle", Float64, queue_size=10)
        self.blade_height_velocity_pub = rospy.Publisher("/grader/controls/blade_height_velocity", Float64, queue_size=10)
        self.blade_tilt_pub = rospy.Publisher("/grader/controls/blade_tilt", Float64, queue_size=10)
        self.blade_angle_pub = rospy.Publisher("/grader/controls/blade_angle", Float64, queue_size=10)

    def update_key(self, key):
        self.key = key.data

    def set_values(self):
        # s=115, q=113, w=119, e=101, r=114, t=116, y=121, c=99,
        if self.key == 65:
            self.rear_wheels_velocity += WHEELS_VELOCITY_BIT
        elif self.key == 66:
            self.rear_wheels_velocity -= WHEELS_VELOCITY_BIT
        elif self.key == 67:
            self.turn_angle -= TURN_BIT
        elif self.key == 68:
            self.turn_angle += TURN_BIT
        elif self.key == 115:
            self.rear_wheels_velocity = 0
        elif self.key == 113:
            self.blade_height_velocity += BLADE_VELOCITY_BIT
        elif self.key == 119:
            self.blade_height_velocity -= BLADE_VELOCITY_BIT
        elif self.key == 101:
            self.blade_angle += 0.1
        elif self.key == 114:
            self.blade_angle -= 0.1
        elif self.key == 116:
            self.blade_tilt += 0.1
        elif self.key == 121:
            self.blade_tilt -= 0.1
        elif self.key == 99:
            self.blade_tilt = 0
            self.blade_angle = 0
            self.blade_height_velocity = DEFAULT_BLADE_VELOCITY
            self.rear_wheels_velocity = 0
            self.turn_angle = 0

        self.key = ''

        self.rear_wheels_velocity = correct_data(self.rear_wheels_velocity, -REAR_WHEELS_VELOCITY_LIMIT,
                                                 REAR_WHEELS_VELOCITY_LIMIT)
        self.turn_angle = correct_data(self.turn_angle, -TURN_LIMIT, TURN_LIMIT)
        self.blade_height_velocity = correct_data(self.blade_height_velocity, LOW_BLADE_HEIGHT_VELOCITY,
                                                  HIGH_BLADE_HEIGHT_VELOCITY)

        try:
            self.turn_direction = self.turn_angle / abs(self.turn_angle)
        except ZeroDivisionError:
            self.turn_direction = 1

    def publish_data(self):
        self.rear_wheels_velocity_pub.publish(data=self.rear_wheels_velocity)
        self.turn_angle_pub.publish(data=self.turn_angle)
        self.blade_height_velocity_pub.publish(data=self.blade_height_velocity)
        self.blade_tilt_pub.publish(data=self.blade_tilt)
        self.blade_angle_pub.publish(data=self.blade_angle)

    def run_transmitter(self):
        while not rospy.is_shutdown():
            self.set_values()
            self.publish_data()
            self.rate.sleep()


if __name__ == "__main__":
    try:
        x = KeyboardTransmitter()
        x.run_transmitter()
    except rospy.ROSInterruptException:
        pass
