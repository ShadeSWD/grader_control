#!/usr/bin/env python
import math

import rospy
from sensor_msgs.msg import JointState
from gazebo_msgs.msg import ModelStates, LinkStates
from std_msgs.msg import Float64, String
from threading import Lock
from collections import namedtuple

Joint = namedtuple('Joint', 'position velocity effort')
Model = namedtuple('Model', 'pose twist')
Link = namedtuple('Link', 'pose twist')

ZERO_BLADE_HEIGHT_EFFORT = 295.95


def hypotenuse(data_1, data_2):
    data = math.sqrt(data_1 ** 2 + data_2 ** 2)
    return data


class GraderController:
    def __init__(self):
        rospy.init_node('grader_publisher', anonymous=True)
        self.left_front_wheel_holder_pub = rospy.Publisher("/grader/left_front_wheel_holder_position_controller"
                                                           "/command", Float64, queue_size=10)
        self.right_front_wheel_holder_pub = rospy.Publisher("/grader/right_front_wheel_holder_position_controller"
                                                            "/command", Float64, queue_size=10)
        self.left_rear_wheel_pub = rospy.Publisher("/grader/left_rear_wheel_velocity_controller"
                                                   "/command", Float64, queue_size=10)
        self.right_rear_wheel_pub = rospy.Publisher("/grader/right_rear_wheel_velocity_controller"
                                                    "/command", Float64, queue_size=10)
        self.body_to_blade_holder_pub = rospy.Publisher("/grader/body_to_blade_holder_velocity_controller"
                                                        "/command", Float64, queue_size=10)
        self.blade_holder_to_blade_angle_keeper_pub = rospy.Publisher("/grader/blade_holder_to_blade_angle"
                                                                      "_keeper_position_controller/command",
                                                                      Float64, queue_size=10)
        self.blade_angle_keeper_to_blade_pub = rospy.Publisher("/grader/blade_angle_keeper_to_blade_position_controller"
                                                               "/command", Float64, queue_size=10)

        self.rate = rospy.Rate(10)
        self.turn_angle = 0              # [-0.6: 0.6]
        self.turn_direction = 1          # left: 1    right: -1
        self.rear_wheels_velocity = 0    # [-3; 3]
        self.blade_angle = 0             # [-0.5; 0.5]
        self.blade_tilt = 0              # [-0.45; 1]
        self.blade_height_velocity = 0

        self.left_front_wheel_turn_msg = ''
        self.right_front_wheel_turn_msg = ''
        self.rear_wheels_velocity_msg = ''
        self.blade_height_msg = ''
        self.blade_tilt_msg = ''
        self.blade_angle_msg = ''

        # get data from controller
        rospy.Subscriber("/grader/controls/rear_wheels_velocity", Float64, self.update_rear_wheels_velocity)
        rospy.Subscriber("/grader/controls/turn_angle", Float64, self.update_turn_angle)
        rospy.Subscriber("/grader/controls/blade_height_velocity", Float64, self.update_blade_height_velocity)
        rospy.Subscriber("/grader/controls/blade_tilt", Float64, self.update_blade_tilt)
        rospy.Subscriber("/grader/controls/blade_angle", Float64, self.update_blade_angle)

        # Get data from gazebo
        self._joint_lock = Lock()
        self._model_states_lock = Lock()
        self._link_states_lock = Lock()

        self.test_pub = rospy.Publisher("/grader/test_pub", String, queue_size=10)

        rospy.Subscriber("/grader/joint_states", JointState, self.update_joints_state)
        rospy.Subscriber("/gazebo/model_states", ModelStates, self.update_grader_state)
        rospy.Subscriber("/gazebo/link_states", LinkStates, self.update_link_states)

        # for grader joints
        self.joint_states = {}
        self.joint_name = []
        self.joint_position = []
        self.joint_velocity = []
        self.joint_effort = []

        # for gazebo models
        self.model_states = {}
        self.model_name = []
        self.model_pose = []
        self.model_twist = []

        # for gazebo links
        self.link_states = {}
        self.link_name = []
        self.link_pose = []
        self.link_twist = []

        # blade_height_publisher
        self.left_blade_mh_pub = rospy.Publisher("/grader/left_blade_mh", Float64, queue_size=10)
        self.right_blade_mh_pub = rospy.Publisher("/grader/right_blade_mh", Float64, queue_size=10)
        self.central_blade_mh_pub = rospy.Publisher("/grader/central_blade_mh", Float64, queue_size=10)

    def update_rear_wheels_velocity(self, msg):
        self.rear_wheels_velocity = msg.data

    def update_turn_angle(self, msg):
        self.turn_angle = msg.data

    def update_blade_height_velocity(self, msg):
        self.blade_height_velocity = msg.data

    def update_blade_tilt(self, msg):
        self.blade_tilt = msg.data

    def update_blade_angle(self, msg):
        self.blade_angle = msg.data

    def update_link_states(self, msg):
        self._link_states_lock.acquire()
        self.link_name = msg.name
        self.link_pose = msg.pose
        self.link_twist = msg.twist

        i = 0
        while i < len(self.link_name):
            dct = {str(self.link_name[i]): Link(pose=self.link_pose[i], twist=self.link_twist[i])}
            self.link_states.update(dct)
            i += 1

        self._link_states_lock.release()

    def update_grader_state(self, msg):
        self._model_states_lock.acquire()
        self.model_name = msg.name
        self.model_pose = msg.pose
        self.model_twist = msg.twist

        i = 0
        while i < len(self.model_name):
            dct = {str(self.model_name[i]): Model(pose=self.model_pose[i], twist=self.model_twist[i])}
            self.model_states.update(dct)
            i += 1

        self._model_states_lock.release()

    def update_joints_state(self, msg):
        self._joint_lock.acquire()
        self.joint_name = msg.name
        self.joint_position = msg.position
        self.joint_velocity = msg.velocity
        self.joint_effort = msg.effort

        i = 0
        while i < len(self.joint_name):
            dct = {str(self.joint_name[i]): Joint(position=self.joint_position[i], velocity=self.joint_velocity[i],
                                                  effort=self.joint_effort[i])}
            self.joint_states.update(dct)
            i += 1

        self._joint_lock.release()

    def publish_grader_commands(self):
        self.blade_tilt_msg = -1 * self.blade_tilt
        self.blade_angle_msg = self.blade_angle
        self.left_front_wheel_turn_msg = self.turn_angle
        self.right_front_wheel_turn_msg = self.turn_angle
        self.rear_wheels_velocity_msg = self.rear_wheels_velocity
        self.blade_height_msg = self.blade_height_velocity

        # Publish wheels
        try:
            self.left_front_wheel_holder_pub.publish(data=self.left_front_wheel_turn_msg)
            self.right_front_wheel_holder_pub.publish(data=self.right_front_wheel_turn_msg)
        except:
            pass
        try:
            self.left_rear_wheel_pub.publish(data=self.rear_wheels_velocity_msg)
            self.right_rear_wheel_pub.publish(data=self.rear_wheels_velocity_msg)
        except:
            pass

        # Publish blade
        try:
            self.body_to_blade_holder_pub.publish(data=self.blade_height_msg)
        except:
            pass
        self.blade_holder_to_blade_angle_keeper_pub.publish(data=self.blade_tilt_msg)
        self.blade_angle_keeper_to_blade_pub.publish(data=self.blade_angle_msg)

        # Publish blade markers height
        try:
            self.left_blade_mh_pub.publish(data=self.link_states['robot::left_blade_marker'].pose.position.z)
            self.right_blade_mh_pub.publish(data=self.link_states['robot::right_blade_marker'].pose.position.z)
            self.central_blade_mh_pub.publish(data=self.link_states['robot::central_blade_marker'].pose.position.z)
        except KeyError:
            pass

    def publish_any_data(self):
        try:
            data = str(hypotenuse(self.model_states['robot'].twist.linear.x, self.model_states['robot'].twist.linear.y))
            self.test_pub.publish(data=data)
        except KeyError:
            pass

    def run_grader_controller(self):
        while not rospy.is_shutdown():
            self.publish_grader_commands()
            self.publish_any_data()
            self.rate.sleep()


if __name__ == "__main__":
    try:
        x = GraderController()
        x.run_grader_controller()
    except rospy.ROSInterruptException:
        pass
