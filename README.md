# grader_control

## About

Software to control a grader

## Grader 3D model

- The package can be installed from the appropriate folder. It includes a simple grader model and a keyboard controller
- You may need to install extra ROS packages if catkin fails

## Ground 3D model

- The custom ground plane contains a bug, so the simulator is launched with a standard ground plane instead
- This package contains a mesh file creator
- With this package a custom ground plane can be created. Follow the instructions in the script mesh_creator.py